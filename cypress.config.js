const { defineConfig } = require('cypress')

module.exports = defineConfig({
  projectId: '7587bd',
  video: false,
  e2e: {
    setupNodeEvents(on, config) {
      return require('./cypress/plugins/index.js')(on, config)
    },
  },
})
